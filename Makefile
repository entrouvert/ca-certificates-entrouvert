#
# Makefile
#

NAME=ca-certificates-entrouvert
VERSION=`git describe | tr - . | cut -c2-`
FULLNAME=$(NAME)-$(VERSION)

LOCALCERTSDIR = /usr/local/share/ca-certificates

all:

install:
	mkdir -p $(DESTDIR)/$(LOCALCERTSDIR); \
	$(MAKE) -C local install LOCALCERTSDIR=$(DESTDIR)/$(LOCALCERTSDIR)


# eobuilder targets -- see https://git.entrouvert.org/eobuilder.git/tree/README.rst
dist-bzip2:
	rm -rf build dist
	mkdir -p build/$(FULLNAME) sdist
	for i in *; do \
		if [ "$$i" != "build" ]; then \
			cp -R "$$i" build/$(NAME)-$(VERSION); \
		fi; \
	done
	cd build && tar cfj ../sdist/$(FULLNAME).tar.bz2 .
	rm -rf build

clean:
	rm -rf sdist build

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(FULLNAME))

package:
	rm -rf debian sdist
	make dist-bzip2
	git checkout debian debian
	cd sdist; mv $(FULLNAME).tar.bz2 $(NAME)_$(VERSION).orig.tar.bz2; tar xvjf $(NAME)_$(VERSION).orig.tar.bz2
	cp -R debian sdist/$(FULLNAME)/
	cd sdist/$(FULLNAME); dch -v $(VERSION)-1 "New upstream release"; dpkg-buildpackage -uc -us
	rm -rf sdist/$(FULLNAME)
	git rm -r --cached debian
	rm -rf debian
	@(echo $(VERSION))
